#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import socket
import multiprocessing
from time import sleep


try:
    import argparse
except Exception:
    print("Please install the dependency first")
    print("You can install them by typing pip install -r requirements.txt\n")
    exit()


# Check the current version of python, if not python 3.x, exit.
if sys.version_info[0] != 3:
    print("This script must be run with Python version 3.x")
    exit()


ut1 = "ipdirectedbroadcast -c COUNT -d "
ut2 = " DESTINATION -p PORT -r RATE -s SIZE"
usage = "\n\t" + ut1 + ut2 + "\n"
example = """For Example:

ipdirectedbroadcast -c 100000 -d 192.168.15.255 -p 5064 -r 14 -s 80

"""


parser = argparse.ArgumentParser(
    usage=usage, epilog=example, formatter_class=argparse.RawDescriptionHelpFormatter
)

parser.add_argument(
    "-c  ", dest="count", type=int, help="Number of datagrams/packets to send."
)

parser.add_argument(
    "-d  ",
    dest="destination",
    type=str,
    help="Broadcast address. Expects an IPv4 broadcast address.",
)

parser.add_argument(
    "-p  ", dest="port", type=int, help="EPICS UDP port. Default is port 5064."
)

parser.add_argument("-r  ", dest="rate", type=int, help="Hertz (rate).")

parser.add_argument("-s  ", dest="size", type=int, help="Payload size (bytes).")


args = parser.parse_args()


# Prints help if no argument is passed
if not len(sys.argv) > 1 or "--help" in sys.argv or "-h" in sys.argv:
    parser.print_help()
    exit()


# Checks if the count is specified or count flag is set, when -c flag is used
if not args.count:
    print("usage: " + usage)
    print("ipdirectedbroadcast -h for more info")
    exit()

# Checks if the desitnation is specified when -d flag is used
if not args.destination:
    print("usage: " + usage)
    print("ipdirectedbroadcast -h for more info")
    exit()

# Checks if the port is specified when -p flag is used
if not args.port:
    print("usage: " + usage)
    print("ipdirectedbroadcast -h for more info")
    exit()

# Checks if the rate is specified when -r flag is used
if not args.rate:
    print("usage: " + usage)
    print("ipdirectedbroadcast -h for more info")
    exit()

# Checks if the size is specified when -s flag is used
if not args.size:
    print("usage: " + usage)
    print("ipdirectedbroadcast -h for more info")
    exit()


broadcast_addr = args.destination
bcAddress = broadcast_addr
bcPort = args.port
bcCount = args.count
bcRate = args.rate
bcSize = args.size


def sendUDP():
    """Send UDP datagram"""
    b = multiprocessing.current_process()
    # assemble payload see bcSize
    payload = ""
    for x in range(0, bcSize):
        payload += "X"
    # s = time.time()
    rateS = 1 / float(bcRate)
    send = "Sending "
    udp = " UDP segments to "
    pid = " PID("
    end = ") every "
    print(
        send
        + str(bcCount)
        + udp
        + str(bcAddress)
        + ":"
        + str(bcPort)
        + pid
        + str(b.pid)
        + end
        + str(rateS)
        + "s"
    )
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    # send at specified rate
    for x in range(bcCount):
        sock.sendto(payload.encode(), (bcAddress, bcPort))
        sleep(rateS)


def run():
    multiprocessing.Process(target=sendUDP).start()


if __name__ == "__main__":
    run()
